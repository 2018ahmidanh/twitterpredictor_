import os
from tweepy.error import RateLimitError
from twitter_collect.collect_candidate_tweet_activity import get_retweets_of_candidate
def get_candidate_queries(num_candidate, file_path):
    """
    Generate and return a list of string queries for the search Twitter API from the file file_path_num_candidate.txt
    :param num_candidate: the number of the candidate
    :param file_path: the path to the keyword and hashtag
    files
    :return: (list) a list of string queries that can be done to the search API independently
    """
    t=''
    file_name_keywords = 'keywords_candidate_' + str(num_candidate)
    file_name_hashtags = 'hashtag_candidate_' + str(num_candidate)
    path_keywords = os.path.join(file_path, file_name_keywords)
    path_hashtags = os.path.join(file_path, file_name_hashtags)

    try:
        keywords = open(path_keywords).readlines()
        hashtags = open(path_hashtags).readlines()
        print('The loaded keywords : {}'.format(keywords))
        print('The loaded hashtags : {}'.format(hashtags))

        return keywords+hashtags

    except IOError:
        print('The file cannot be opened, please check the given path / condidate number')

######################################################################
if __name__ == "__main__":
    username = 'EmmanuelMacron'
    retweets_per_tweet = get_retweets_of_candidate('EmmanuelMacron')
    for tweet, nb_rt in retweets_per_tweet.items():
        print('{}, the number of retweets are --> {}'.format(tweet, nb_rt))




