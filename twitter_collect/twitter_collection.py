from twitter_collect.twitter_connection_setup import twitter_setup
from twitter_collect.collect_candidate_actuality_tweets import get_candidate_queries, get_tweets_from_candidates_search_queries
import json
import tweepy


def serialize_tweets(tweet):
    """
    Transforms a object tweet in a simple dictionary
    :param tweet: a tweet in the original Tweepy object
    :return: (dict) a dictionary with basics information of the tweet
    """
    if isinstance(tweet, tweepy.models.Status):
        return {"text": str(tweet.text),
                "id": tweet.id,
                "retweeted": tweet.retweeted,
                "retweet_count": tweet.retweet_count,
                "favorite_count": tweet.favorite_count,
                "created_at": tweet.created_at.isoformat(),
                "followers_count": tweet.user.followers_count,
                "description": str(tweet.user.description),
                "name": tweet.user.name}
    raise TypeError(repr(tweet) + " Not the same object type")


def store_tweets(queries, filename):
    """
    Makes a search of tweets with the subjects in the list of queries, and save them in filename, with JSON format
    :param queries: list of strings to search
    :param filename: filename of the file to store the data
    :return: nothing
    """
    json_to_dump = []

    for query in queries:
        print(query.__class__)
        for tweet in query:
            json_to_dump.append(serialize_tweets(tweet))

    with open(filename, 'w', encoding='utf-8') as f:
        json.dump(json_to_dump, f, indent=4)


if __name__ == "__main__":
    num_candidate = 1
    queries = get_candidate_queries(num_candidate, '../CandidateData')
    api_instance = twitter_setup()
    queries_results = get_tweets_from_candidates_search_queries(queries, api_instance)
    output_filename = '../jsonDump/tweets_candidate_'+ str(num_candidate) + '.json'
    store_tweets(queries_results, output_filename)
