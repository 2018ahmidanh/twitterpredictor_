import pandas as pd


def transform_to_dict(tweet):
    return {"tweet_textual_content": str(tweet.text),
            "len" : len(tweet.text),
            "likes": tweet.favorite_count,
            "RTs": tweet.retweet_count,
            "date": tweet.created_at.isoformat()}


def transform_to_dataframe(tweets):
    dict_list = []

    for tweet in tweets:
        dict_list.append(transform_to_dict(tweet))
    data_frame = pd.DataFrame(dict_list)

    return data_frame
