#collect  retweets of the candidat's tweets
#imporoved version

from tweepy import *
from twitter_collect.twitter_connection_setup import twitter_setup

#def get_retweets_of_candidate(user_id):              #getting the retweets with the candidat's id
#    connexion = twitter_setup()
#    statuses = connexion.user_timeline(id = user_id, count = 20)
#    retweets_per_tweet = {}
#    for status in statuses:
#        if (not status.retweeted) or ('RT @' not in status.text):
#            retweets_per_tweet[status.text] = status.retweet_count #counting retweets of the candidat's tweet
#    return retweets_per_tweet



def get_retweets_of_candidate(candidate_username):
    """
    Gets the lastest tweets from candidate and show how many retweets it had
    :param candidate_username: username of the account to analyze
    :return: (dict) for each text as key, the total number of retweets the tweet had
    """
    connection = twitter_setup()
    tweets = connection.user_timeline(screen_name=candidate_username, count=200)
    retweets_per_tweet = {}

    for status in tweets:
        # Only for original tweets, excluding the retweets
        if (not status.retweeted) or ('RT @' not in status.text):
            retweets_per_tweet[status.text] = status.retweet_count

    return retweets_per_tweet

#collect candidat's tweets

def candidate_tweets(candidate_username):
    connection = twitter_setup()
    tweets = connection.user_timeline(screen_name=candidate_username, count=200)
    original_tweets = []

    for status in tweets:
        # Only for original tweets, excluding the retweets
        if (not status.retweeted) or ('RT @' not in status.text):
            original_tweets.append(status)

    return original_tweets



######################################################################
if __name__ == "__main__":
    username = 'EmmanuelMacron'
    retweets_per_tweet = get_retweets_of_candidate('EmmanuelMacron')
    for tweet, nb_rt in retweets_per_tweet.items():
        print('{}, the number of retweets are --> {}'.format(tweet, nb_rt))
