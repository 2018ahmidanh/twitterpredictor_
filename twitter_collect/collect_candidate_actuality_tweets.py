#ce programme collecte les tweets ecrites par le candidat
#on a fait avec user parce qu'on a pas arrivé à l'aide search

from tweepy import *
from twitter_collect.twitter_connection_setup import twitter_setup
import os
from tweepy.error import RateLimitError
from twitter_collect.twitter_connection_setup import twitter_setup


def get_candidate_queries(num_candidate, file_path):
    """
    Generate and return a list of string queries for the search Twitter API from the file file_path_num_candidate.txt
    :param num_candidate: the number of the candidate
    :param file_path: the path to the keyword and hashtag files
    :return: (list) a list of string queries that can be done to the search API independently
    """
    file_name_keywords = 'keywords_candidate_' + str(num_candidate) + '.txt'
    file_name_hashtags = 'hashtag_candidate_' + str(num_candidate) + '.txt'
    path_keywords = os.path.join(file_path, file_name_keywords)
    path_hashtags = os.path.join(file_path, file_name_hashtags)

    try:
        keywords = open(path_keywords).readlines()
        hashtags = open(path_hashtags).readlines()
        print('The loaded keywords : {}'.format(keywords))
        print('The loaded hashtags : {}'.format(hashtags))

        return keywords+hashtags

    except IOError:
        print('The file cannot be opened')


def get_tweets_from_candidates_search_queries(queries, twitter_api):
    """
    Searches for each query in list, all the tweets which contains the subject.
    :param queries: list of strings, which are the terms you want to search
    :param twitter_api: API connection to do the search
    :return: (list) list of tweets.
    """
    results = []
    try:
        for query in queries:
            query_result = twitter_api.search(query, language="english", rpp=100)
            results += query_result
    except RateLimitError:
        print('An error occurred during the query')

    return results

######################################################################
if __name__ == '__main__':
    num_candidate = 1
    queries = get_candidate_queries(num_candidate, "../CandidateData")
    api = twitter_setup()
    results = get_tweets_from_candidates_search_queries(queries, api)
    for tweet in results:
        print(tweet.text)
