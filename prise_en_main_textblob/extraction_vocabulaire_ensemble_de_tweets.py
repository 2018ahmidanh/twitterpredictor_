from textblob import TextBlob

#On suppose qu'on dispose d'un datagramme contenant les infos de l'ensemble de tweets, comme dans les étapes précédentes
def extraction(data):
    liste_mots = []
    for texte in data["tweet_textual_content"].values:
        wiki = TextBlob(texte)
        wiki_correct = wiki.correct()
        for mot in wiki_correct.words:
            mot_lemmatisé = mot.lemmatize()
            if not mot_lemmatisé in liste_mots:
                liste_mots.append(mot_lemmatisé)
    return liste_mots

