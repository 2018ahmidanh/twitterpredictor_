import numpy as np

def most_liked(data):
    nb_like_max  = np.max(data['Like'])
    rt  = data[data.Like == nb_like_max].index[0]

# Most liked:
    print("The tweet with more Likes is: \n{}".format(data['tweet_textual_content'][rt]))
    print("Number of likes: {}".format(nb_like_max))
    print("{} characters.\n".format(data['len'][rt]))
