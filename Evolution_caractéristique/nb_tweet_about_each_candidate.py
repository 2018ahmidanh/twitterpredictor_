import pandas as pd
import matplotlib.pyplot as plt

def nb_tweet_par_candidat(data):

    #On sélectionne les tweets à propos de chacun des candidats
    tweets_Macron = data["@EmmanuelMacron" in data.tweet_textual_content.split(' ') or "#EmmanuelMacron" in data.tweet_textual_content.split(' ')]
    tweets_MarineLePen = data["@MarineLePen" in data.tweet_textual_content.split(' ') or "#MarineLePen" in data.tweet_textual_content.split(' ')]
    tweets_JLMelenchon = data["@JeanLucMelenchon" in data.tweet_textual_content.split(' ') or "#JeanLucMelenchon" in data.tweet_textual_content.split(' ') or "@JLMelenchon" in data.tweet_textual_content.split(' ') or "#JLMelenchon" in data.tweet_textual_content.split(' ')]
    tweets_FrancoisFillon = data["@FrancoisFillon" in data.tweet_textual_content.split(' ') or "#FrancoisFillon" in data.tweet_textual_content.split(' ')]
    tweets_BenoitHamon = data["@BenoitHamon" in data.tweet_textual_content.split(' ') or "#BenoitHamon" in data.tweet_textual_content.split(' ')]
    tweets_EdouardPhilippe = data["@EdouardPhilippe" in data.tweet_textual_content.split(' ') or "#EdouardPhilippe" in data.tweet_textual_content.split(' ')]

    donnees_Macron = pd.Series(data = tweets_Macron["RTs"].values, index = tweets_Macron["Date"])
    donnees_MarineLePen = pd.Series(data = tweets_MarineLePen["RTs"].values, index = tweets_MarineLePen["Date"])
    donnees_JLMelenchon = pd.Series(data = tweets_JLMelenchon["RTs"].values, index = tweets_JLMelenchon["Date"])
    donnees_FrancoisFillon = pd.Series(data = tweets_FrancoisFillon["RTs"].values, index = tweets_FrancoisFillon["Date"])
    donnees_BenoitHamon = pd.Series(data = tweets_BenoitHamon["RTs"].values, index = tweets_BenoitHamon["Date"])
    donnees_EdouardPhilippe = pd.Series(data = tweets_EdouardPhilippe["RTs"].values, index = tweets_EdouardPhilippe["Date"])

    #visualisation
    donnees_Macron.plot(figsize=(16,4), label="RT_Macron",legend=True)
    donnees_MarineLePen.plot(figsize=(16,4), label="RT_MarineLePen",legend=True)
    donnees_JLMelenchon.plot(figsize=(16,4), label="RT_JLMelenchon",legend=True)
    donnees_FrancoisFillon.plot(figsize=(16,4), label="RT_FrancoisFillon",legend=True)
    donnees_BenoitHamon.plot(figsize=(16,4), label="RT_BenoitHamon",legend=True)
    donnees_EdouardPhilippe.plot(figsize=(16,4), label="RT_EdouardPhilippe",legend=True)

    plt.show()

